package com.example.zhenua.weather

import retrofit2.Call
import retrofit2.http.*


const val apiKey = "0d84c2a391816bfb436d1883bd77ce96"
data class WeathetInf(val location: Time, val current: WeatherInfo)
data class WeatherInfo(val observation_time: String,
                       val temperature: Int,
                       val weather_descriptions: List<String>,
                       val wind_speed: Int,
                       val wind_dir: String,
                       val pressure: Int,
                       val precip: Float,
                       val feelslike: Int)
data class Time (val localtime: String)
interface MessageApi
{

    @GET("current?access_key=0d84c2a391816bfb436d1883bd77ce96&query=Tomsk")

    fun messages(@Query("access_key") key: String, @Query("query") town: String): Call<WeathetInf>


}


