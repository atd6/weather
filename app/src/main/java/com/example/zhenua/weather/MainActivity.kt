package com.example.zhenua.weather

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory




class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        val list = arrayListOf("Moscow", "Tomsk","Amsterdam","New York", "Saint Petersburg","Seoul","Jakarta", "Cairo")
        super.onCreate(savedInstanceState)
        val messagesApi = createSiteApi()

        setContentView(R.layout.activity_main)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val messages = messagesApi.messages(apiKey, list[position])
                messages.enqueue( object: Callback<WeathetInf>{
                    override fun onResponse(call: Call<WeathetInf>, response: Response<WeathetInf>) {
                        var time = response.body()!!.location.localtime
                        time = time.substring(time.length-5)
                        timeInfo.text = "Погода в $time"
                        tempInfo.text = response.body()!!.current.temperature.toString() + "° C"
                        realFeelInfo.text = "Real feel:\n\n" + response.body()!!.current.feelslike + "° C"
                        pressureInfo.text = "Pressure:\n\n" + response.body()!!.current.pressure.toString() + " mbar"
                        windInfo.text = "Wind:\n\n" + response.body()!!.current.wind_dir + " " +
                                response.body()!!.current.wind_speed + " km/h"
                        predictInfo.text = "Probability of Precipitation:\n\n" + response.body()!!.current.precip + "%"
                        var desc = ""
                        for (i in 0 until response.body()!!.current.weather_descriptions.size){
                            desc = desc + response.body()!!.current.weather_descriptions[i] + " "
                        }
                        weatherDescriptions.text = desc
                    }

                    override fun onFailure(call: Call<WeathetInf>, t: Throwable) {
                        weatherDescriptions.text = t.toString()
                    }
                })
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }







    }
    fun createSiteApi(): MessageApi{
        val site = Retrofit.Builder()
            .baseUrl("http://api.weatherstack.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
         return site.create(MessageApi::class.java)
    }

}
